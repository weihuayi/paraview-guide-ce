Contributing to ParaView Guide - Community Edition
==================================================

Contritbutions are always welcome. There are several ways of contributing.

1.  You can may your contributions through **[merge requests][]** made to this [GitLab][]
repository.

2.  You can also post your edits to the [Mailing List][], coordinating with any of
the ParaView developers to get your updates in.

3.  Alternatively, you can also post your text changes directly on a new issue on the
[issue tracker] or in a [snippet].

For those interested in making changes through the **[merge requests][]**, the workflow
is as follows:

1.  Register with [GitLab Access] to create an account and select a user name.

2.  [Fork ParaViewGuide-CE][] into your user's namespace on GitLab.

3.  Clone the repository:

        $ mkdir ParaViewGuide; cd ParaViewGuide
        $ git clone https://gitlab.kitware.com/paraview/paraview-guide-ce.git src
        $ cd src
    The main repository will be configured as your `origin` remote.
    
4.  Add your fork as a the `gitlab` remote.

        $ git remote add gitlab https://gitlab.kitware.com/username/paraview-guide-ce.git
    You can use configure the remote to use SSH access instead, if needed.

5.  Install dependencies and [build](build.md)

        $ mkdir ../build; cd ../build
        $ ccmake ../src
        $ make

6.  Edit files and create commits (repeat as needed):

        $ git checkout -b my-topic origin/master
        $ edit file1 file2 file3
        $ git add file1 file2 file3
        $ git commit
        
7.  Push commits in your topic branch to your fork in GitLab:

        $ git push gitlab HEAD
        
8.  Visit your fork in GitLab, browse to the "**Merge Requests**" link on the
    left, and use the "**New Merge Request**" button in the upper right to
    create a Merge Request.

Please refer to [ContributorNotes.tex][] to guidelines on contributing content
for the guide.

[merge requests]: https://gitlab.kitware.com/paraview/paraview-guide-ce/merge_requests
[GitLab]: https://gitlab.kitware.com/paraview/paraview-guide-ce/
[Mailing List]: http://www.paraview.org/mailing-lists/
[ContributorNotes.tex]: ParaView/ContributorNotes.tex
[GitLab Access]: https://gitlab.kitware.com/users/sign_in
[Fork ParaViewGuide-CE]: https://gitlab.kitware.com/paraview/paraview-guide-ce/fork/new
[issue tracker]: https://gitlab.kitware.com/paraview/paraview-guide-ce/issues
[snippet]: https://gitlab.kitware.com/paraview/paraview-guide-ce/snippets